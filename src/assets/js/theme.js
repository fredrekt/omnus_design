tailwind.config = {
	theme: {
		extend: {
			colors: {
				primary: '#111727',
				gray: '#F7F7F7',
				lightgray: '#595C62',
				lighterGray: '#C6C6C6',
				blackDefault: '#0C0C0C',
				blackDark: '#2C2C2C',
				blackTertiary: `#434343`,
				lightTertiary: `#8E8E8E`,
				blackShade: '#464646',
				activeNavColor: `rgba(217, 217, 217, 0.15)`,
				unpaidStatus: '#EBEBEB',
				inProgressBg: '#FFF6C6',
				inProgressTxt: '#DEBB00'
			}
		}
	}
};
