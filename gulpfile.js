const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const connect = require('gulp-connect');
const del = require('del');
const htmlmin = require('gulp-htmlmin');
const runSequence = require('run-sequence');
const uglify = require('gulp-uglify');
const csso = require('gulp-csso');
const autoprefixer = require('gulp-autoprefixer');

const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

gulp.task('hello', function() {
    console.log('Hello Zell');
});

// Gulp task to minify CSS files
gulp.task('styles', function () {
    return gulp.src('./src/assets/css/styles.css')
      // Auto-prefix css styles for cross browser compatibility
      .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
      // Minify the file
      .pipe(csso())
      // Output
      .pipe(gulp.dest('./dist/css'))
});

gulp.task('pages', function() {
    return gulp.src(['./src/pages/*.html', '*.html'])
      .pipe(htmlmin({
        collapseWhitespace: true,
        removeComments: true
      }))
      .pipe(gulp.dest('./dist'));
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
    return gulp.src('./src/assets/js/**/*.js')
    // Minify the file
    .pipe(uglify())
    // Output
    .pipe(gulp.dest('./dist/js'))
});

gulp.task('copy', function () {
	return gulp
		.src(['./src/pages/*', './src/assets/*', '*.html'], {
			base: './src/'
		})
		.pipe(gulp.dest('./dist/'));
});

gulp.task('clean', () => del(['dist']));

gulp.task('serve', function(done) {
    connect.server({
      root: './',
      livereload: true
    });
    done();
});